// function for sorting
// @method sortListDir


function sortListDir() {
  var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
  list = document.getElementById("audio-listing");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  //Make a loop that will continue until no switching has been done:
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    //Loop through all list-items:
    for (i = 0; i < (b.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*check if the next item should switch place with the current item,
      based on the sorting direction (asc or desc):*/
      if (dir == "asc") {
        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
          /*if next item is alphabetically lower than current item,
          mark as a switch and break the loop:*/
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (b[i].innerHTML.toLowerCase() < b[i + 1].innerHTML.toLowerCase()) {
          /*if next item is alphabetically higher than current item,
          mark as a switch and break the loop:*/
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
      //Each time a switch is done, increase switchcount by 1:
      switchcount ++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}


// function for all operations in player page
// @method setDataForAudioPlayer
function setDataForAudioPlayer()
{
    var audio_data=[];
    $("#name-sort").click(function () {
      sortListDir();
    });

    $('.list-files').on('click', 'li', function () {
      var fileid = $(this).attr("id").split("file")[0];
      $(".track-item,.audio-title").text(audio_data[fileid].title);
      $(".audio-desc").text(audio_data[fileid].description);
      $("#jquery_jplayer_1").jPlayer("setMedia", { mp3: "assets/audio/" + audio_data[fileid].file }).jPlayer("play");
    });

    $.ajax({
      url: "assets/data/playerdata.json",
      type: 'GET',
      dataType: 'json', // added data type
      success: function (res) {
        $("#file-length").text(res.length);
        for (var i = 0; i < res.length; i++) {
          var title =res[i].title;
          var text = title + '<button class="download"> Download </button>';
          if (text.length) {
            var checkbox = document.createElement('div');
         
           var option= $('<li id="' + i + 'file"></li>').html(text).appendTo('ul.list-files');
           option.prepend(checkbox);
          }
        }
        audio_data=res;
      }
    });

    $(".jp-next").click(function () {
      var currentTime = $('#jquery_jplayer_1').data('jPlayer').status.currentTime;
      if (currentTime > 0) {
        $("#jquery_jplayer_1").jPlayer("play", $("#jquery_jplayer_1").data('jPlayer').status.currentTime + 15);
      }
    });

    $(".jp-previous").click(function () {
      var currentTime = $('#jquery_jplayer_1').data('jPlayer').status.currentTime;
      if (currentTime > 15) {
        $("#jquery_jplayer_1").jPlayer("play", $("#jquery_jplayer_1").data('jPlayer').status.currentTime - 15);
      }
    });


    $("#jquery_jplayer_1").jPlayer({
      ready: function () {
        $(this).jPlayer("setMedia", {
          title: "Bubble",
          mp3: ""
        });
      },
      swfPath: "dist/jplayer",
      supplied: "mp3",
      wmode: "window",
      useStateClassSkin: true,
      autoBlur: false,
      smoothPlayBar: true,
      keyEnabled: true,
      remainingDuration: true,
      toggleDuration: true
    });
}